from flask import Flask, render_template, url_for, request, redirect
from flask_restful import reqparse
from lib.Search import Search
from lib.Document import Document
from lib.Pagination import Pagination
import datetime
import hashlib

app = Flask(__name__)


@app.route('/')
def search():
    """
    Index, website main page, a simple search
    """
    return render_template('search.html')


@app.route('/advanced')
def advanced():
    """
    Advanced search page, all features available
    """
    return render_template('advanced.html')


@app.route('/results', methods=['GET'])
def results():
    """
    Display search results for both simple search and advanced search
    searchBy: Excepting 'keywords'(default) or 'file'
    *searchFor: What this search by.  If searchBy is 'keyword', this is keywords, or this is file name
    category: Which category to search, search in all categories by default if not specified
    sort: Sort results by which attribute, excepting 'relevance'(default) or 'date'
    page: Excepting a int number
    resultsPerPage: Excepting a int number
    yearFrom, monthFrom, dayFrom, yearTo, monthTo, dayTo: Search within which date region
    """
    parser = reqparse.RequestParser()
    parser.add_argument('searchBy', 'keywords')
    parser.add_argument('searchFor', required=True)
    parser.add_argument('category', 'all')
    parser.add_argument('sort', 'relevance')
    parser.add_argument('page', 1, type=int)
    parser.add_argument('resultsPerPage', 50, type=int)
    parser.add_argument('yearFrom', None, type=int)
    parser.add_argument('monthFrom', None, type=int)
    parser.add_argument('dayFrom', None, type=int)
    parser.add_argument('yearTo', None, type=int)
    parser.add_argument('monthTo', None, type=int)
    parser.add_argument('dayTo', None, type=int)
    args = parser.parse_args()

    assert args['searchBy'] in ['keywords', 'file']
    if args['searchBy'] == 'keywords':
        search = args['searchFor']
    else:
        with open('./upload/' + args['searchFor']) as file:
            search = file.read()

    if len(args['searchFor']) < 30:
        keywords_display = args['searchFor']
    else:
        keywords_display = args['searchFor'][:50] + '...'

    if args['searchBy'] == 'keywords':
        collection = Search(search).commit()
    else:
        collection = Search(search, 'document').commit()
    collection = collection.in_category(args['category'])
    collection.sort(args['sort'])

    if args['yearFrom'] is not None and args['monthFrom'] is not None and args['dayFrom'] is not None \
            and args['yearTo'] is not None and args['monthTo'] is not None and args['dayTo'] is not None:
        date1 = datetime.datetime(args['yearFrom'], args['monthFrom'], args['dayFrom'])
        date2 = datetime.datetime(args['yearTo'], args['monthTo'], args['dayTo'])
        collection = collection.between(date1, date2)

    pagination = Pagination('results', args, collection.get_page_quantity(rpp=args['resultsPerPage']))

    '''
    make two copies of present arguments, change one attribute in each of them, 
    and use to generate url for different sort methods
    '''
    args_relevance = args.copy()
    args_relevance['sort'] = 'relevance'
    args_date = args.copy()
    args_date['sort'] = 'date'
    sort_by = {
        'relevance': {
            'url': url_for('results', **args_relevance),
            'style': ' active' if args['sort'] == 'relevance' else '',
        },
        'date': {
            'url': url_for('results', **args_date),
            'style': ' active' if args['sort'] == 'date' else '',
        },
    }

    return render_template('results.html',
                           high=True if args['searchBy'] == 'keywords' else False,
                           keywords=args['searchFor'] if args['searchBy'] == 'keywords' else '(uploaded file)',
                           keywords_display=keywords_display if args['searchBy'] == 'keywords' else '(uploaded file)',
                           quantity=collection.quantity(),
                           sort_by=sort_by,
                           results=collection.get_page(args['page'], args['resultsPerPage']),
                           pages=pagination.get_pages(args['page']))


@app.route('/advanced/search', methods=['POST'])
def advance_search():
    """
    Parameters same as result display, this page is only used to upload file, save it,
    and redirect to result page to search by it
    """
    parser = reqparse.RequestParser()
    parser.add_argument('searchBy', required=True)
    parser.add_argument('keywords', None)
    parser.add_argument('category', required=True)
    parser.add_argument('resultsPerPage', required=True, type=int)
    parser.add_argument('yearFrom', required=True, type=int)
    parser.add_argument('monthFrom', required=True, type=int)
    parser.add_argument('dayFrom', required=True, type=int)
    parser.add_argument('yearTo', required=True, type=int)
    parser.add_argument('monthTo', required=True, type=int)
    parser.add_argument('dayTo', required=True, type=int)
    args = parser.parse_args()
    args_send = args.copy()
    args_send.pop('keywords', None)

    assert args['searchBy'] in ['keywords', 'file']
    if args['searchBy'] == 'keywords':
        args_send['searchFor'] = args['keywords']
    else:
        '''
        if one file was uploaded, read its content, save to ./upload, name by its md5 value
        '''
        file = request.files['file']
        content = file.stream.read().decode().replace('\n', ' ')
        file_name = hashlib.md5(content.encode('utf-8')).hexdigest()
        with open('./upload/' + file_name, 'w+') as up_file:
            up_file.write(content)
        args_send['searchFor'] = file_name
    url = url_for('results', **args_send)
    return redirect(url)


@app.route('/case/<document_name>')
def view(document_name):
    """
    Read a document and display by its name
    :param document_name: document file name
    """
    view_document = Document(document_name)
    return render_template('view.html', content=view_document.content_html)


if __name__ == '__main__':
    app.run()
