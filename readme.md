## !! IMPORTANT !!
Codes of the website is complete, but **additional files are required** to run this website:

Download NSWSC files from https://www.dropbox.com/s/xkx9y2kso8qun97/

Extract all files to somewhere, and set that path in config.py

Run

python3 app.py
