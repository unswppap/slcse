import lib.TF_IDF_Similarity as SimilarKeywordDocuments
from lib.Document import Document
from lib.DocumentCollection import DocumentCollection
from lib.Cache import Cache
import json


class Search:
    """
    Search documents
    """

    def __init__(self, search_for, mode='keywords'):
        """
        Initializer
        :param search_for: keywords
        :param mode: maybe by keyword or document
        """
        assert mode in ['keywords', 'document']
        self.search_for = search_for
        self.collection = DocumentCollection()
        self.mode = mode

    def commit(self):
        """
        Search all documents by keywords
        :return: a result collection
        """
        cache = Cache(target=self.search_for)
        if cache.exist:
            self.collection = DocumentCollection(recovery=cache.get())
        else:
            with open('data/output_article_index.min.json', 'r') as file:
                article_index = file.read()
            article_index = json.loads(article_index)

            if self.mode == 'keywords':
                documents = SimilarKeywordDocuments.get_by_words(self.search_for)
                for key, similarity in documents:
                    if similarity > 0:
                        file_name = article_index[str(key)]
                        one_document = Document(file_name, mode='list', index=str(key), search_for=self.search_for, relevance=similarity)
                        if one_document.valid:
                            self.collection.add_document(one_document)
            else:
                documents = SimilarKeywordDocuments.get_by_words2(self.search_for)
                for key, similarity in documents:
                    if similarity > 0:
                        file_name = article_index[str(key)]
                        one_document = Document(file_name, mode='list', index=str(key), search_for=self.search_for, relevance=similarity)
                        if one_document.valid:
                            self.collection.add_document(one_document)
                # documents = SimilarDocuments.search(self.search_for)
                # for key, similarity in documents.items():
                #     file_name = article_index[str(key)]
                #     one_document = Document(file_name, mode='list', index=str(key), search_for=self.search_for, relevance=similarity)
                #     if one_document.valid:
                #         self.collection.add_document(one_document)
            cache.store(self.collection.serialize())
        return self.collection
